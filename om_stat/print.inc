#if defined _file_om_utility_included
	#endinput
#endif
#define _file_om_utility_included

#pragma newdecls required

stock void printInfo(char[] str, ...) {
	char msg[135] = "[OMOC] ";
	char buffer[128];

	VFormat(buffer, sizeof(buffer), str, 2);
	StrCat(msg, sizeof(msg), buffer);

	for (int i = 1; i <= MaxClients; i++) {
		if (IsClientConnected(i) && IsClientInGame(i) && !IsFakeClient(i)) {
			PrintToConsole(i, msg);
		}
	}
}