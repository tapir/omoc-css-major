#if defined _file_om_game_tracker_included
	#endinput
#endif
#define _file_om_game_tracker_included

#include <cstrike>
#include "database"

#pragma newdecls required

#define MAXCLIENTS 65

enum struct PlayerData {
	int		healthLeft;
	int		armorLeft;
	int		damagesFrom[MAXCLIENTS];

	void Init() {
		this.healthLeft	= 100;
		this.armorLeft	= 100;
		for (int i = 0; i < MAXCLIENTS; i++) {
			this.damagesFrom[i] = 0;
		}
	}
}

enum struct RoundData {
	int count;
	int cDeathCounter;
	int tDeathCounter;
	int clutchLevel;
	int clutchTeam;
	int clutchPlayer;
	bool clutchPossibility;

	void Init() {
		this.count				= 1;
		this.cDeathCounter		= 0;
		this.tDeathCounter		= 0;
		this.clutchLevel		= 0;
		this.clutchPlayer		= 0;
		this.clutchTeam			= CS_TEAM_NONE;
		this.clutchPossibility	= false;
	}

	void OnRoundEnd() {
		this.Init();
		this.count = CS_GetTeamScore(CS_TEAM_T) + CS_GetTeamScore(CS_TEAM_CT) + 1;
	}

	void IncreaseDeath(int team, int teamPop) {
		if (team == CS_TEAM_CT) {
			this.cDeathCounter++;
		} else if (team == CS_TEAM_T) {
			this.tDeathCounter++;
		}
		if (this.clutchPossibility == false) {
			if (this.cDeathCounter == teamPop - 1 && teamPop - this.tDeathCounter > 1) {
				// Find the last alive
				for (int i = 1; i <= MaxClients; i++) {
					if (!IsClientInGame(i) || !IsClientConnected(i) || GetClientTeam(i) != CS_TEAM_CT) {
						continue;
					}
					if (IsPlayerAlive(i)) {
						this.clutchPlayer = GetSteamAccountID(i, true);
					}
				}
				this.clutchLevel		= teamPop - this.tDeathCounter;
				this.clutchPossibility	= true;
				this.clutchTeam			= CS_TEAM_CT;
			} else if (this.tDeathCounter == teamPop - 1 && teamPop - this.cDeathCounter > 1) {
				// Find the last alive
				for (int i = 1; i <= MaxClients; i++) {
					if (!IsClientInGame(i) || !IsClientConnected(i) || GetClientTeam(i) != CS_TEAM_T) {
						continue;
					}
					if (IsPlayerAlive(i)) {
						this.clutchPlayer = GetSteamAccountID(i, true);
					}
				}
				this.clutchLevel		= teamPop - this.cDeathCounter;
				this.clutchPossibility	= true;
				this.clutchTeam			= CS_TEAM_T;
			}
		}
	}
}

enum struct GameTracker {
	ArrayList		players;
    ConVar			mpWinLimit;
    ConVar			mpMaxRounds;
	ConVar			omStatTeamLimit;
	ConVar			omStatDebug;
	RoundData		round;
	int				gameId;

	void Init() {
		this.round.Init();
		this.ResetPlayers();

		this.gameId				= GetURandomInt();
		this.mpWinLimit			= FindConVar("mp_winlimit");
		this.mpMaxRounds		= FindConVar("mp_maxrounds");
		this.omStatTeamLimit	= FindConVar("om_stat_teamlimit");
		this.omStatDebug		= FindConVar("om_stat_debug");
	}

	void Clean() {
		if (this.players != null) {
			this.players.Clear();
		}
	}

	void Reset() {
		this.Clean();
		this.Init();
	}

	void ResetPlayers() {
		if (this.players != null) {
			this.players.Clear();
		}
		this.players = new ArrayList(sizeof(PlayerData), MaxClients+1);
		for (int i = 1; i <= MaxClients; i++) {
			PlayerData p;
			p.Init();
			this.SetPlayer(i, p)
		}
	}

	void OnPlayerHurt(int victim, int attacker, int healthLeft, int armorLeft, int damage) {
		PlayerData p;
		this.GetPlayer(victim, p);
		p.damagesFrom[attacker] += damage;
		p.healthLeft = healthLeft;
		p.armorLeft	= armorLeft;
		this.SetPlayer(victim, p);
	}

	void OnPlayerDeath(int victim, int attacker, int victimTeam, int attackerTeam) {
		// assist event
		for(int k = 1; k <= MaxClients; k++) {
			if (!IsClientInGame(k) || !IsClientConnected(k) || k == attacker || k == victim) {
				continue;
			}
			PlayerData p;
			this.GetPlayer(victim, p);
			if (p.damagesFrom[k] >= 40) {
				int steamId = GetSteamAccountID(k, true);
				// if not fake user
				if(!IsFakeClient(k)) {
					dbSimpleEvent("player_assist", steamId, attackerTeam, this.round.count, this.gameId);
				}
				// debug
				if(this.omStatDebug.IntValue == 1) {
					PrintToServer("[player_assist] steamId: %d team: %d", steamId, attackerTeam);
				}
			}
		}

		// clutch preparation
		this.round.IncreaseDeath(victimTeam, this.omStatTeamLimit.IntValue);
	}

	void OnRoundEnd(int winner) {
		// clutch event
		if (this.round.clutchPossibility == true) {
			int clutchStatus  = this.round.clutchTeam == winner;

			// if not fake user
			if (this.round.clutchPlayer > 0) {
				dbPlayerClutch(this.round.clutchPlayer, this.round.clutchTeam, this.round.clutchLevel, clutchStatus, this.round.count, this.gameId);
			}
			// debug
			if(this.omStatDebug.IntValue == 1) {
				PrintToServer("[player_clutch] steamId: %d, team: %d, level: %d, status: %d", this.round.clutchPlayer, this.round.clutchTeam, this.round.clutchLevel, clutchStatus);
			}
		}

		// match end event
		int tscore = CS_GetTeamScore(CS_TEAM_T);
		int cscore = CS_GetTeamScore(CS_TEAM_CT);
		if (this.mpWinLimit.IntValue != 0 && (tscore >= this.mpWinLimit.IntValue || cscore >= this.mpWinLimit.IntValue || this.round.count >= this.mpMaxRounds.IntValue)) {
			// find a player from each team
			int tplayer = -1;
			int cplayer = -1;
			for (int i = 1; i <= MaxClients; i++) {
				// if user is fake or not relevent
				if (!IsClientInGame(i) || !IsClientConnected(i) || IsFakeClient(i)) {
					continue
				}
				int steamId	= GetSteamAccountID(i);
				int team 	= GetClientTeam(i);

				if (team == CS_TEAM_CT) {
					cplayer = steamId;
				} else if (team == CS_TEAM_T) {
					tplayer = steamId;
				}

				// check if we've got everything we want
				if (tplayer != -1 && cplayer != -1) {
					break;
				}
			}

			if (winner == CS_TEAM_T) {
				dbMatchEnd(tscore, cscore, tplayer, cplayer, this.gameId);
			} else {
				dbMatchEnd(cscore, tscore, cplayer, tplayer, this.gameId);
			}
			// debug
			if(this.omStatDebug.IntValue == 1) {
				PrintToServer("[match_end] winScore: %d, lossScore: %d, winPlayer: %d, lossPlayer: %d", tscore, cscore, tplayer, cplayer);
			}
		}

		// player survived event
		for (int i = 1; i <= MaxClients; i++) {
			if (IsClientInGame(i) && IsClientConnected(i) && IsPlayerAlive(i)) {
				int steamId		= GetSteamAccountID(i, true);
				int team 		= GetClientTeam(i);

				// if user is not fake
				if (steamId > 0) {
					dbSimpleEvent("player_survived", steamId, team, this.round.count, this.gameId);
				}

				// debug
				if(this.omStatDebug.IntValue == 1) {
					PrintToServer("[player_survived] steamId: %d, team: %d", steamId, team);
				}
			}
		}

		this.ResetPlayers();
		this.round.OnRoundEnd();
	}

	void GetPlayer(int clientId, PlayerData p) {
		this.players.GetArray(clientId, p);
	}

	void SetPlayer(int clientId, PlayerData p) {
		this.players.SetArray(clientId, p);
	}
}