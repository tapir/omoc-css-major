#if defined _file_om_database_included
	#endinput
#endif
#define _file_om_database_included

#include <dbi>

#include "unixtime"

#pragma newdecls required

Database datab = null;
const bool DB_DEBUG = false;

stock void dbConnect() {
	SQL_TConnect(dbConnect_, "css_major");
}

stock void dbDisconnect() {
	delete datab;
	printInfo("[database] Database connection terminated");
}

public void dbConnect_(Handle owner, Handle hndl, const char[] error, any data){
	if(hndl == null) {
		printInfo("[database] Can't connect to database: %s", error);
		PrintToServer("[database] Can't connect to database: %s", error);
		return;
	}
	datab = hndl;
	printInfo("[database] Database connection established");
}

public void dbLogError_(Handle owner, Handle hndl, const char[] error, any data) {
	if (strcmp(error, "") != 0) {
		printInfo("[database] Can't connect to database: %s", error);
		PrintToServer("[database] Error: %s", error);
	}
}

stock void dbHurtEvent(
	int victim,
	int attacker,
	int victimTeam,
	int attackerTeam,
	const char[] weapon,
	int hLeft,
	int aLeft,
	int hDmg,
	int aDmg,
	int hitbox,
	int round,
	int gameId
) {
	char hbox[16]; hitboxName(hitbox, hbox, sizeof(hbox));
	char vicTeamStr[16]; teamNameStr(victimTeam, vicTeamStr, sizeof(vicTeamStr));
	char attTeamStr[16]; teamNameStr(attackerTeam, attTeamStr, sizeof(attTeamStr));
	char tstamp[32]; timestamp(tstamp, sizeof(tstamp));

	char buffer[256];
	Format(
		buffer,
		sizeof(buffer),
		"CALL css_major.dbHurtEvent(%d, %d, '%s', '%s', %d, %d, '%s', %d, %d, %d, %d, '%s', '%s');",
		victim, attacker, vicTeamStr, attTeamStr, round, gameId, weapon, hLeft, aLeft, hDmg, aDmg, hbox, tstamp
	);

	if (DB_DEBUG) {
		PrintToServer("%s", buffer);
	}

	SQL_TQuery(datab, dbLogError_, buffer);
}

stock void dbKillEvent(
	int victim,
	int attacker,
	int victimTeam,
	int attackerTeam,
	int dominated,
	int revenge,
	bool headshot,
	const char[] weapon,
	int round,
	int gameId
) {
	char dom[6]; intToBoolStr(dominated, dom, sizeof(dom));
	char rev[6]; intToBoolStr(revenge, rev, sizeof(rev));
	char hshot[6]; boolToStr(headshot, hshot, sizeof(hshot));
	char vicTeamStr[16]; teamNameStr(victimTeam, vicTeamStr, sizeof(vicTeamStr));
	char attTeamStr[16]; teamNameStr(attackerTeam, attTeamStr, sizeof(attTeamStr));
	char tstamp[32]; timestamp(tstamp, sizeof(tstamp));

	char buffer[256];
	Format(
		buffer,
		sizeof(buffer),
		"CALL css_major.dbKillEvent(%d, %d, '%s', '%s', %d, %d, %s, %s, %s, '%s', '%s');",
		victim, attacker, vicTeamStr, attTeamStr, round, gameId, hshot, dom, rev, weapon, tstamp
	);

	if (DB_DEBUG) {
		PrintToServer("%s", buffer);
	}

	SQL_TQuery(datab, dbLogError_, buffer);
}

stock void dbSimpleEvent(const char[] type, int player, int team, int round, int gameId) {
	char teamStr[16]; teamNameStr(team, teamStr, sizeof(teamStr));
	char tstamp[32]; timestamp(tstamp, sizeof(tstamp));

	char buffer[256];
	Format(
		buffer,
		sizeof(buffer),
		"CALL css_major.dbSimpleEvent('%s', %d, '%s', %d, %d, '%s');",
		type, player, teamStr, round, gameId, tstamp
	);

	if (DB_DEBUG) {
		PrintToServer("%s", buffer);
	}

	SQL_TQuery(datab, dbLogError_, buffer);
}

stock void dbPlayerClutch(int attacker, int team, int clutchLevel, int clutchStatus, int round, int gameId) {
	char status[6]; intToBoolStr(clutchStatus, status, sizeof(status));
	char teamStr[16]; teamNameStr(team, teamStr, sizeof(teamStr));
	char tstamp[32]; timestamp(tstamp, sizeof(tstamp));

	char buffer[256];
	Format(
		buffer,
		sizeof(buffer),
		"CALL css_major.dbClutchEvent(%d, '%s', %d, %d, %d, %s, '%s');",
		attacker, teamStr, round, gameId, clutchLevel, status, tstamp
	);

	if (DB_DEBUG) {
		PrintToServer("%s", buffer);
	}

	SQL_TQuery(datab, dbLogError_, buffer);
}

stock void dbMatchEnd(int winScore, int lossScore, int winPlayer, int lossPlayer, int gameId) {
	char tstamp[32]; timestamp(tstamp, sizeof(tstamp));

	char buffer[256];
	Format(
		buffer,
		sizeof(buffer),
		"CALL css_major.dbMatchEvent(%d, %d, %d, %d, %d, '%s');",
		gameId, winPlayer, lossPlayer, winScore, lossScore, tstamp
	);

	if (DB_DEBUG) {
		PrintToServer("%s", buffer);
	}

	SQL_TQuery(datab, dbLogError_, buffer);
}


stock void teamNameStr(int team, char[] output, int size) {
    if (team == CS_TEAM_CT) {
        strcopy(output, size, "counter");
    } else if (team == CS_TEAM_T) {
        strcopy(output, size, "terror");
    } else {
        strcopy(output, size, "spectator");
    }
}

stock void intToBoolStr(int x, char[] output, int size) {
	if (x > 0) {
		strcopy(output, size, "true");
	} else {
		strcopy(output, size, "false");
	}
}

stock void boolToStr(bool x, char[] output, int size) {
	if (x) {
		strcopy(output, size, "true");
	} else {
		strcopy(output, size, "false");
	}
}

stock void hitboxName(int hitbox, char[] output, int size) {
	if (hitbox == 1) {
		strcopy(output, size, "head");
	} else if (hitbox == 2) {
		strcopy(output, size, "chest");
	} else if (hitbox == 3) {
		strcopy(output, size, "stomach");
	} else if (hitbox == 4) {
		strcopy(output, size, "left_arm");
	} else if (hitbox == 5) {
		strcopy(output, size, "right_arm");
	} else if (hitbox == 6) {
		strcopy(output, size, "left_leg");
	} else if (hitbox == 7) {
		strcopy(output, size, "right_leg");
	} else {
		strcopy(output, size, "unknown");
	}
}

stock void timestamp(char[] output, int size) {
	int year;
	int month;
	int day;
	int hour;
	int minute;
	int second;
	int stamp = GetTime();
	UnixToTime(stamp, year, month, day, hour, minute, second);
	Format(output, size, "%04d-%02d-%02d %02d:%02d:%02d", year, month, day, hour, minute, second);
}