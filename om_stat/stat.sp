#include <sourcemod>
#include <sdktools>
#include <float>
#include <string>
#include <clients>
#include <cstrike>

#include "print"
#include "database"
#include "game_tracker"


public Plugin myinfo = {
	name        = "OMOC CSS Major Stats Tracker",
	author      = "Cosku Bas",
	description = "Tracks player stats for the tournament OMOC CSS Major",
	version     = "1.0",
	url         = ""
};

// -----------------------
// ---- global state  ----
// -----------------------

GameTracker game;
ConVar omStatDebug      = null;
ConVar omStat           = null;
ConVar omStatTeamLimit  = null;
ConVar mpRestartGame    = null;

// --------------------------
// ---- plugin callbacks ----
// --------------------------

public void OnPluginStart() {
	omStatTeamLimit	= CreateConVar("om_stat_teamlimit", "3", "Set population per team", FCVAR_NOTIFY);
	omStatDebug		= CreateConVar("om_stat_debug", "0", "Enable/Disable OMOC Stats Tracker debug messages", FCVAR_NOTIFY);
	omStat			= CreateConVar("om_stat", "0", "Enable/Disable OMOC Stats Tracker", FCVAR_NOTIFY);
	if (omStat != INVALID_HANDLE) {
		HookConVarChange(omStat, omStatChange);
	}

	mpRestartGame = FindConVar("mp_restartgame");
	if (mpRestartGame != INVALID_HANDLE) {
		HookConVarChange(mpRestartGame, mpRestartGameChange);
	}

	resetGame();
}

public void OnPluginEnd() {
	// Just in case om_stat was not disabled
	unhookEvents();
	dbDisconnect();
}

public void OnMapStart() {
	resetGame();
}

public void OnClientConnected(int clientId) {
	// debug
	if (omStatDebug.IntValue == 1) {
		printInfo("[player_connect] New client connected with IDs: %d, %d", clientId, GetSteamAccountID(clientId, true));
		PrintToServer("[player_connect] clientId: %d, steamId: %d", clientId, GetSteamAccountID(clientId, true));
	}
}

// ---------------------------------
// ---- convar change callbacks ----
// ---------------------------------

// for some reason, source engine event 'game_start' doesn't work so this is the workaround
public void mpRestartGameChange(Handle cvar, const char[] oldVal, const char[] newVal) {
	if (StringToInt(newVal, 10) > 0) {
		resetGame();
	}
}

public void omStatChange(Handle cvar, const char[] oldVal, const char[] newVal) {
	if (strcmp(oldVal, "0", true) == 0 && strcmp(newVal, "1", true) == 0) {
		printInfo("[convar_change] OM Stat activated");
		PrintToServer("[convar_change] OM Stat activated");
		dbConnect();
		hookEvents();
	} else if (strcmp(oldVal, "1", true) == 0 && strcmp(newVal, "0", true) == 0) {
		printInfo("[convar_change] OM Stat deactivated");
		PrintToServer("[convar_change] OM Stat deactivated");
		unhookEvents();
		dbDisconnect();
	}
}

// ------------------------------------------------
// ---- event callbacks that sends data to SQL ----
// ------------------------------------------------

public void eventRoundEnd(Event event, const char[] name, bool dontBroadcast) {
	int winner = event.GetInt("winner");
	game.OnRoundEnd(winner);
}

public void eventSimple(Event event, const char[] name, bool dontBroadcast) {
	int user		= GetClientOfUserId(event.GetInt("userid"));
	int userTeam	= GetClientTeam(user);
	int userSteamId = GetSteamAccountID(user, true);

	if (!IsFakeClient(user)) {
		dbSimpleEvent(name, userSteamId, userTeam, game.round.count, game.gameId);
	}

	// debug
	if(omStatDebug.IntValue == 1) {
		PrintToServer("[%s] steamId: %d", name, userSteamId);
	}
}

public void eventPlayerDeath(Event event, const char[] name, bool dontBroadcast) {
	int user            = GetClientOfUserId(event.GetInt("userid"));
	int attacker        = GetClientOfUserId(event.GetInt("attacker"));
	int userTeam        = GetClientTeam(user);
	int userSteamId     = GetSteamAccountID(user, true);
	int attackerTeam    = GetClientTeam(attacker);
	int attackerSteamId = GetSteamAccountID(attacker, true);
	int dominated       = event.GetInt("dominated");
	int revenge         = event.GetInt("revenge");
	bool headshot       = event.GetBool("headshot");
	char weapon[64];    event.GetString("weapon", weapon, 64);

	if (!(IsFakeClient(user) || IsFakeClient(attacker))) {
		dbKillEvent(userSteamId, attackerSteamId, userTeam, attackerTeam, dominated,
					revenge, headshot, weapon, game.round.count, game.gameId);
	}

	// debug
	if(omStatDebug.IntValue == 1) {
		PrintToServer("[player_death] vSteamId: %d, aSteamId: %d", userSteamId, attackerSteamId);
		PrintToServer("[player_death] weapon: %s, headshot: %d, dominated: %d, revenge: %d", weapon, headshot, dominated, revenge);
	}

	// assist and clutch preparation
	game.OnPlayerDeath(user, attacker, userTeam, attackerTeam);
}

public void eventPlayerHurt(Event event, const char[] name, bool dontBroadcast) {
	int user            = GetClientOfUserId(event.GetInt("userid"));
	int attacker        = GetClientOfUserId(event.GetInt("attacker"));
	int userTeam        = GetClientTeam(user);
	int userSteamId     = GetSteamAccountID(user, true);
	int attackerTeam    = GetClientTeam(attacker);
	int attackerSteamId = GetSteamAccountID(attacker, true);
	int health          = event.GetInt("health");
	int armor           = event.GetInt("armor");
	int dmgHealth       = event.GetInt("dmg_health");
	int dmgArmor        = event.GetInt("dmg_armor");
	int hitgroup        = event.GetInt("hitgroup");
	char weapon[64];    event.GetString("weapon", weapon, 64);

	PlayerData p;
	game.GetPlayer(user, p);

	// max damage can't exceed health left
	if (dmgHealth > p.healthLeft) {
		dmgHealth = p.healthLeft;
	}

	// max damage can't exceed armor left
	if (dmgArmor > p.armorLeft) {
		dmgArmor = p.armorLeft;
	}

	if (!(IsFakeClient(user) || IsFakeClient(attacker))) {
		dbHurtEvent(userSteamId, attackerSteamId, userTeam, attackerTeam, weapon, health, armor,
					dmgHealth, dmgArmor, hitgroup, game.round.count, game.gameId);
	}

	// debug
	if(omStatDebug.IntValue == 1) {
		PrintToServer("[player_hurt] vSteamId: %d, aSteamId: %d", userSteamId, attackerSteamId);
		PrintToServer("[player_hurt] remainingHealth: %d, remainingArmor: %d", health, armor);
		PrintToServer("[player_hurt] damageHealth: %d, damageArmor: %d", dmgHealth, dmgArmor);
		PrintToServer("[player_hurt] weapon: %s, hitgroup: %d", weapon, hitgroup);
	}

	// player damages update
	game.OnPlayerHurt(user, attacker, health, armor, dmgHealth);
}

// ---------------------------
// ---- utility functions ----
// ---------------------------

void hookEvents() {
	HookEvent("round_end", eventRoundEnd);
	HookEvent("bomb_planted", eventSimple);
	HookEvent("bomb_defused", eventSimple);
	HookEvent("bomb_exploded", eventSimple);
	HookEvent("bomb_dropped", eventSimple);
	HookEvent("hegrenade_detonate", eventSimple);
	HookEvent("flashbang_detonate", eventSimple);
	HookEvent("smokegrenade_detonate", eventSimple);
	HookEvent("weapon_reload", eventSimple);
	HookEvent("weapon_fire_on_empty", eventSimple);
	HookEvent("player_death", eventPlayerDeath);
	HookEvent("player_hurt", eventPlayerHurt);
}

void unhookEvents() {
	UnhookEvent("round_end", eventRoundEnd);
	UnhookEvent("bomb_planted", eventSimple);
	UnhookEvent("bomb_defused", eventSimple);
	UnhookEvent("bomb_exploded", eventSimple);
	UnhookEvent("bomb_dropped", eventSimple);
	UnhookEvent("hegrenade_detonate", eventSimple);
	UnhookEvent("flashbang_detonate", eventSimple);
	UnhookEvent("smokegrenade_detonate", eventSimple);
	UnhookEvent("weapon_reload", eventSimple);
	UnhookEvent("weapon_fire_on_empty", eventSimple);
	UnhookEvent("player_death", eventPlayerDeath);
	UnhookEvent("player_hurt", eventPlayerHurt);
}

void resetGame() {
	// reset game ID and round counter
	game.Reset();
	printInfo("[game_start] New game started with ID: %d", game.gameId);

	// debug
	PrintToServer("[game_start] gameId: %d", game.gameId);
}