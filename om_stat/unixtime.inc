#if defined _file_unixtime_included
	#endinput
#endif
#define _file_unixtime_included

#pragma newdecls required

stock const int g_iYearSeconds[2] = {
	31536000,	// Normal year
	31622400 	// Leap year
};

stock const int g_iMonthSeconds[12] = {
	2678400, 	// January		31
	2419200, 	// February		28
	2678400, 	// March		31
	2592000, 	// April		30
	2678400, 	// May			31
	2592000, 	// June			30
	2678400, 	// July			31
	2678400, 	// August		31
	2592000, 	// September	30
	2678400, 	// October		31
	2592000, 	// November		30
	2678400  	// December		31
};

stock const int g_iDaySeconds		= 86400;
stock const int g_iHourSeconds		= 3600;
stock const int g_iMinuteSeconds	= 60;

stock void UnixToTime(int iTimeStamp_, int &iYear, int &iMonth, int &iDay, int &iHour, int &iMinute, int &iSecond) {
	int iTemp;
	iYear	= 1970;
	iMonth	= 1;
	iDay	= 1;
	iHour	= 0;

	while (iTimeStamp_ > 0) {
		iTemp = IsLeapYear(iYear);
		if ((iTimeStamp_ - g_iYearSeconds[iTemp]) >= 0) {
			iTimeStamp_ -= g_iYearSeconds[iTemp];
			iYear ++;
		} else {
			break;
		}
	}

	while (iTimeStamp_ > 0) {
		iTemp = SecondsInMonth(iYear, iMonth);
		if ((iTimeStamp_ - iTemp) >= 0) {
			iTimeStamp_ -= iTemp;
			iMonth ++;
		} else {
			break;
		}
	}

	while (iTimeStamp_ > 0) {
		if ((iTimeStamp_ - g_iDaySeconds) >= 0) {
			iTimeStamp_ -= g_iDaySeconds;
			iDay ++;
		} else {
			break;
		}
	}

	while(iTimeStamp_ > 0) {
		if ((iTimeStamp_ - g_iHourSeconds) >= 0) {
			iTimeStamp_ -= g_iHourSeconds;
			iHour ++;
		} else {
			break;
		}
	}

	iMinute = iTimeStamp_ / 60;
	iSecond = iTimeStamp_ % 60;
}

stock int SecondsInMonth(const int iYear, const int iMonth) {
	if (IsLeapYear(iYear) && (iMonth == 2)) {
		return g_iMonthSeconds[iMonth - 1] + g_iDaySeconds;
	}
	return g_iMonthSeconds[iMonth - 1];
}

stock int IsLeapYear(const int iYear)  {
	return ((iYear % 4) == 0) && (((iYear % 100) != 0) || ((iYear % 400) == 0));
}