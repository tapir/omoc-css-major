# **OMOC Plugin Kullanımı**

## CVars
**om_omocpreset 0/1**		: Oyun modlarını OMOC Major ayarlarıyla çalıştırır.  
**om_halftime 0/1**			: İki yarıdan oluşan maç ayarı için toggle.  
**om_otenable 0/1**			: Beraberlik durumunda uzatma (overtime) oynanmasını aktive/deaktive eder.  
**om_otrounds #rounds**		: Uzatmalarda bir yarıda oynanacak round sayısını belirler. (Ör: om_otrounds 3) (Min: 1, Max: 10)  
**om_otstartmoney #money**	: Uzatmalarda pistol rounddaki başlangıç parasını belirler. (Ör: om_otstartmoney 10000) (Min: 800, Max: 16000)  

## Commands
**om_swapnow**					: Tüm oyuncuların takımlarını değiştirir.  
**om_pause**					: Tüm oyunu pause eder. Tekrar kullanımı oyunu devam ettirir.  
**om_revive #userid**			: Hedef userid oyuncusunu hayata döndürür.  
**om_swapuser #userid**			: Hedef userid oyuncusunun takımını değiştirir.  
**om_setroundtime #s**			: Round saatini girilen saniyeye set eder.
**om_usermoney #userid #money**	: Hedef userid oyuncusunun parasını verilen miktara eşitler.

**om_disable**	: Plugini kapatır.  
**om_status**	: Print current parameters and guard values to console.  

**om_warmup**		: Warmup modunu başlatır.  
**om_knife**		: Bıçak roundu başlatır.  
**om_match**		: Maç modunu başlatır.  

## Presets (Aliases)
**om_major**	: OMOC Major turnuva ayarlarıyla warmup başlatır.  
**om_csgo**		: CSGO ayarlarıyla maç başlatır. (Max 30 rounds, beraberlik var.)  
**om_csgoot**	: CSGO ayarlarıyla, beraberlikte uzayan maç başlatır. (MR30 - MR6 Overtime $10000 Start money on OT)  
**om_csgok**	: CSGO ayarlarıyla, knife roundu olan maç başlatır.  
**om_csgokot**	: CSGO ayarlarıyla, beraberlikte uzayan ve knife roundu olan maç başlatır. (MR30 - MR6 Overtime $10000 Start money on OT)  
**om_omocmac**	: OMOC Major ayarlarıyla maç başlatır. (Max 20 rounds, beraberlik ve knife round var.)  
**om_fun**		: Deathmatch modunu başlatır. 


Knife round sonunda, sunucu otomatik olarak yeni bir warmup başlatarak, kazanan takımdan herhangi bir oyuncunun chat kısmını (say) **!stay** ya da **!switch** yazmasını bekler. Bu mesajın gelmesi ya da roundtime'ın sona ermesi (!stay istenildiği varsayılır) durumunda maç otomatik olarak başlar.


------------

Planned Features:
- Round start save settings to restart the round
	- money at the round start event after bonus awarded from previous round.
	- pistol, rifle and utilities.
	- Kevlar and helmet.
	- Defuse kit.
	- Team scores
	- Player scores