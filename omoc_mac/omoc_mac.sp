#pragma semicolon 1

#include <sourcemod>
#include <sdktools>
#include <cstrike>

enum gameModes {
    Disabled = 0,
    Warmup,
    Knife,
    StaySwitch,
    Match,
}

ConVar g_cvStartMoney;
ConVar g_cvRoundTime;
ConVar g_cvFreezeTime;
ConVar g_cvBuyTime;
ConVar g_cvWinLimit;
ConVar g_cvMaxRounds;
ConVar g_cvAmmoFlashMax;
ConVar g_cvHalftime;
ConVar g_cvOmocPreset;
ConVar g_cvServerPausable;
ConVar g_cvOTEnable;
ConVar g_cvOTRounds;
ConVar g_cvOTStartMoney;

bool is_warmup = false;
bool is_knife = false;
bool is_match = false;
bool is_stayswitch = false;
bool officialMatch = false;
bool omocpreset = false;
bool savedSettings = false;

bool otEnabled = false;
bool otSwap = false;
bool otResetToPistol = false;
bool is_overtime;
int otHalfTimeCheck;
int roundToCheckOT;
int otRounds = 3;
int otStartMoney;
int curRnd;

bool firsthalf = false;
bool swap = false;
bool halftime;

int g_iAccount = -1;

int roundEndReason;
int previousRoundWinner = 0; //Values: 2 T - 3 CT
int currentRoundWinner;

bool consec2WinCT = false;
bool consec2WinT = false;
int consecLossCT = 0;
int consecLossT = 0;

int playerMoney[65];
bool playerAlive[65];

int roundLossBonus[5] = {1400, 1900, 2400, 2900, 3400};
int roundLossBonus2[4] = {1500, 2000, 2500, 3000};

char mapname[30];

int startmoney;
float roundtime;
int freezetime;
float buytime;
int winlimit;
int maxrounds;
int ammoflashmax;

int saved_startmoney;
float saved_roundtime;
int saved_freezetime;
float saved_buytime;
int saved_winlimit;
int saved_maxrounds;

public Plugin:myinfo = {
	name = "OMOC CS:S MatchManager",
	author = "OMOC",
	description = "Manages Counter-Strike:Source matches in a similar way to official csgo tournament matches.",
	version = "1.0.0",
	url = ""
};

public OnPluginStart() {

    g_cvHalftime    = CreateConVar("om_halftime", "1", "Determines whether the match switches sides in a halftime event.", FCVAR_NOTIFY, true, 0.0, true, 1.0);
    g_cvOmocPreset  = CreateConVar("om_omocpreset", "0", "Determines if OMOC Major settings will be applied for game modes.", FCVAR_NOTIFY, true, 0.0, true, 1.0);
    g_cvOTEnable    = CreateConVar("om_otenable", "0", "Determines if overtime mode is enabled.", FCVAR_NOTIFY, true, 0.0, true, 1.0);
    g_cvOTRounds    = CreateConVar("om_otrounds", "3", "Number of rounds that will be played on the first half of the overtime.", FCVAR_NOTIFY, true, 1.0, true, 10.0);
    g_cvOTStartMoney = CreateConVar("om_otstartmoney", "10000", "Start money for the overtime.", FCVAR_NOTIFY, true, 800.0, true, 16000.0);

    //Low level commands
    RegAdminCmd("om_warmup", Cmd_OmWarmup, ADMFLAG_GENERIC, "Starts the warmup on the current map.");
    RegAdminCmd("om_knife", Cmd_OmKnife, ADMFLAG_GENERIC, "Starts the knife round on the current map.");
    RegAdminCmd("om_stayswitch", Cmd_OmStaySwitch, ADMFLAG_GENERIC, "Starts the choice period after the knife round on the current map.");
    RegAdminCmd("om_match", Cmd_OmMatch, ADMFLAG_GENERIC, "Starts the match on the current map. (OMOC Major Settings)");
    RegAdminCmd("om_status", Cmd_Status, ADMFLAG_GENERIC, "Print the current setting of the plugin to the client console.");
    
    RegAdminCmd("om_swapnow", Cmd_Swap, ADMFLAG_GENERIC, "Swaps the teams instantly.");
    RegAdminCmd("om_revive", Cmd_Revive, ADMFLAG_GENERIC, "Resurrects the given userid instantly.");
    RegAdminCmd("om_pause", Cmd_Pause, ADMFLAG_GENERIC, "Freezes all players and game clock instantly.");
    RegAdminCmd("om_swapuser", Cmd_SwapUser, ADMFLAG_GENERIC, "Swaps the team of the given userid instantly.");
    RegAdminCmd("om_setroundtime", Cmd_SetRoundTime, ADMFLAG_GENERIC, "Sets the current round time to the given seconds value.");
    RegAdminCmd("om_usermoney", Cmd_UserMoney, ADMFLAG_GENERIC, "Sets the money for given userid instantly.");

    //Easy access commands
    RegAdminCmd("om_disable", Cmd_OmDisable, ADMFLAG_GENERIC, "Disable omoc plugin.");
    RegAdminCmd("om_csgo", Cmd_CSGO, ADMFLAG_GENERIC, "Start the match on current map with CSGO Official Matchmaking settings.");
    RegAdminCmd("om_csgoot", Cmd_CSGOOT, ADMFLAG_GENERIC, "Start the match on current map with MR30 and MR6 overtimes with 10000 start money.");
    RegAdminCmd("om_csgok", Cmd_CSGOK, ADMFLAG_GENERIC, "Start knife round on the current map which will then continue with Official Matchmaking settings for the match.");
    RegAdminCmd("om_csgokot", Cmd_CSGOKOT, ADMFLAG_GENERIC, "Start knife round on the current map which will then continue with MR30 and MR6 overtimes with 10000 start money.");
    RegAdminCmd("om_major", Cmd_Major, ADMFLAG_GENERIC, "Start warmup for OMOC Major Tournment match.");
    RegAdminCmd("om_omocmac", Cmd_OMOCMac, ADMFLAG_GENERIC, "Start match on the current map for fun with OMOC Major settings.");
    RegAdminCmd("om_fun", Cmd_Fun, ADMFLAG_GENERIC, "Start deathmatch.");

    RegConsoleCmd("sm_stay", Cmd_Stay);
    RegConsoleCmd("sm_switch", Cmd_Switch);

    //Get CVARs
    g_cvStartMoney 	    = FindConVar("mp_startmoney");
    g_cvRoundTime	    = FindConVar("mp_roundtime");
    g_cvFreezeTime	    = FindConVar("mp_freezetime");
    g_cvBuyTime	        = FindConVar("mp_buytime");
    g_cvWinLimit	    = FindConVar("mp_winlimit");
    g_cvMaxRounds	    = FindConVar("mp_maxrounds");
    g_cvAmmoFlashMax	= FindConVar("ammo_flashbang_max");
    g_cvServerPausable  = FindConVar("sv_pausable");
    //Cash
    g_iAccount	= FindSendPropOffs("CCSPlayer", "m_iAccount");

    //Hook ConVars
    HookConVarChange(g_cvStartMoney, ConVarChange);
    HookConVarChange(g_cvMaxRounds, ConVarChange);
    HookConVarChange(g_cvHalftime, ConVarChange);
    HookConVarChange(g_cvRoundTime, ConVarChange);
    HookConVarChange(g_cvFreezeTime, ConVarChange);
    HookConVarChange(g_cvBuyTime, ConVarChange);
    HookConVarChange(g_cvWinLimit, ConVarChange);
    HookConVarChange(g_cvAmmoFlashMax, ConVarChange);
    HookConVarChange(g_cvOmocPreset, ConVarChange);
    HookConVarChange(g_cvOTEnable, ConVarChange);
    HookConVarChange(g_cvOTRounds, ConVarChange);
    HookConVarChange(g_cvOTStartMoney, ConVarChange);

    //Assign cvars to variables
    halftime    = GetConVarBool(g_cvHalftime);
    omocpreset  = GetConVarBool(g_cvOmocPreset);

    otEnabled   = GetConVarBool(g_cvOTEnable);
    otRounds    = GetConVarInt(g_cvOTRounds);
    otStartMoney = GetConVarInt(g_cvOTStartMoney);

    startmoney	= GetConVarInt(g_cvStartMoney);
    maxrounds	= GetConVarInt(g_cvMaxRounds);
    roundtime	= GetConVarFloat(g_cvRoundTime);
    freezetime	= GetConVarInt(g_cvFreezeTime);
    buytime	    = GetConVarFloat(g_cvBuyTime);
    winlimit	= GetConVarInt(g_cvWinLimit);
    ammoflashmax = GetConVarInt(g_cvAmmoFlashMax);

    //Hook Events
    HookEvent("player_death", Event_PlayerDeath);
    HookEvent("player_spawn", Event_PlayerSpawn);
    HookEvent("round_start", Event_RoundStart);
    HookEvent("round_end", Event_RoundEnd);
}

public ConVarChange(Handle:cvar, const String:oldValue[], const String:newValue[]) {
    int nval = StringToInt(newValue);
    float fval = StringToFloat(newValue);

    if (cvar == g_cvHalftime) {
        halftime = (nval == 1) ? true : false;
    }
    else if (cvar == g_cvOmocPreset) {
        omocpreset = (nval == 1) ? true : false;
    }
    else if (cvar == g_cvOTEnable) {
        otEnabled = (nval == 1) ? true : false;
    }
    else if (cvar == g_cvOTRounds) {
        otRounds = nval;
    }
    else if (cvar == g_cvOTStartMoney) {
        otStartMoney = nval;
    }
    else if (cvar == g_cvMaxRounds) {
        maxrounds = nval;
    }
    else if (cvar == g_cvStartMoney) {
        startmoney = nval;
    }
    else if (cvar == g_cvFreezeTime) {
        freezetime = nval;
    }
    else if (cvar == g_cvWinLimit) {
        winlimit = nval;
    }
    else if (cvar == g_cvAmmoFlashMax) {
        ammoflashmax = nval;
    }
    else if (cvar == g_cvRoundTime) {
        roundtime = fval;
    }
    else if (cvar == g_cvBuyTime) {
        buytime = fval;
    }
}

public Action Cmd_Status(int client, int args) {
    PrintToConsole(client, "is_warmup: %d", is_warmup);
    PrintToConsole(client, "is_knife: %d", is_knife);
    PrintToConsole(client, "is_stayswitch: %d", is_stayswitch);
    PrintToConsole(client, "is_match: %d", is_match);
    PrintToConsole(client, "Omoc Major Preset: %d", omocpreset);
    PrintToConsole(client, "Halftime: %d", halftime);
    PrintToConsole(client, "OT Enabled: %d", otEnabled);
    PrintToConsole(client, "OT Rounds: %d", otRounds);
    PrintToConsole(client, "OT Start Money: %d", otStartMoney);
    PrintToConsole(client, "Official Match: %d", officialMatch);
    PrintToConsole(client, "Saved Settings: %d", savedSettings);
}

public Action Cmd_OmDisable(int client, int args) {
    SetGameMode(Disabled);
}

public Action Cmd_CSGOOT(int client, int args) {
    g_cvOmocPreset.IntValue = 0;
    g_cvHalftime.IntValue = 1;
    g_cvOTEnable.IntValue = 1;

    g_cvOTRounds.IntValue = 3;
    g_cvOTStartMoney.IntValue = 10000;

    g_cvStartMoney.IntValue = 800;
    g_cvRoundTime.FloatValue = 2.0;
    g_cvFreezeTime.IntValue = 15;
    g_cvBuyTime.FloatValue = 0.34;
    g_cvWinLimit.IntValue = 16;
    g_cvMaxRounds.IntValue = 30;
    g_cvAmmoFlashMax.IntValue = 2;
    savedSettings = false;
    ServerCommand("om_match");
}

public Action Cmd_CSGOKOT(int client, int args) {
    g_cvOmocPreset.IntValue = 0;
    g_cvHalftime.IntValue = 1;
    g_cvOTEnable.IntValue = 1;
    g_cvOTRounds.IntValue = 3;
    g_cvOTStartMoney.IntValue = 10000;
    
    g_cvStartMoney.IntValue = 800;
    g_cvRoundTime.FloatValue = 2.0;
    g_cvFreezeTime.IntValue = 15;
    g_cvBuyTime.FloatValue = 0.34;
    g_cvWinLimit.IntValue = 16;
    g_cvMaxRounds.IntValue = 30;
    g_cvAmmoFlashMax.IntValue = 2;
    savedSettings = false;
    ServerCommand("om_knife");
}

public Action Cmd_OMOCMac(int client, int args) {
    g_cvOmocPreset.IntValue = 0;
    g_cvHalftime.IntValue = 1;
    g_cvOTEnable.IntValue = 0;

    g_cvStartMoney.IntValue = 800;
    g_cvRoundTime.FloatValue = 1.5;
    g_cvFreezeTime.IntValue = 10;
    g_cvBuyTime.FloatValue = 0.34;
    g_cvWinLimit.IntValue = 11;
    g_cvMaxRounds.IntValue = 20;
    g_cvAmmoFlashMax.IntValue = 2;
    ServerCommand("om_knife");
}

public Action Cmd_Fun(int client, int args) {
    g_cvOmocPreset.IntValue = 0;
    g_cvHalftime.IntValue = 0;
    g_cvOTEnable.IntValue = 0;

    ServerCommand("om_warmup");
}

public Action Cmd_Major(int client, int args) {
    g_cvOmocPreset.IntValue = 1;
    g_cvHalftime.IntValue = 1;
    g_cvOTEnable.IntValue = 0;
    ServerCommand("om_stat_teamlimit 3");
    ServerCommand("om_stat_debug 0");

    ServerCommand("om_warmup");
}

public Action Cmd_CSGO(int client, int args) {
    g_cvOmocPreset.IntValue = 0;
    g_cvHalftime.IntValue = 1;
    g_cvOTEnable.IntValue = 0;

    g_cvStartMoney.IntValue = 800;
    g_cvRoundTime.FloatValue = 2.0;
    g_cvFreezeTime.IntValue = 15;
    g_cvBuyTime.FloatValue = 0.34;
    g_cvWinLimit.IntValue = 16;
    g_cvMaxRounds.IntValue = 30;
    g_cvAmmoFlashMax.IntValue = 2;
    savedSettings = false;
    ServerCommand("om_match");
}

public Action Cmd_CSGOK(int client, int args) {
    g_cvOmocPreset.IntValue = 0;
    g_cvHalftime.IntValue = 1;
    g_cvOTEnable.IntValue = 0;

    g_cvStartMoney.IntValue = 800;
    g_cvRoundTime.FloatValue = 2.0;
    g_cvFreezeTime.IntValue = 15;
    g_cvBuyTime.FloatValue = 0.34;
    g_cvWinLimit.IntValue = 16;
    g_cvMaxRounds.IntValue = 30;
    g_cvAmmoFlashMax.IntValue = 2;
    savedSettings = false;
    ServerCommand("om_knife");
}

public Action Cmd_Stay(int client, int args) {
    if (is_stayswitch) {
        if (GetClientTeam(client) == currentRoundWinner) {
            ServerCommand("om_match");
        }
    }
}

public Action Cmd_Switch(int client, int args) {
    if (is_stayswitch) {
        if (GetClientTeam(client) == currentRoundWinner) {
            ServerCommand("om_swapnow");
            ServerCommand("om_match");
        }
    }
}

public Action Cmd_OmWarmup(int client, int args) {
    if (omocpreset) {
        g_cvRoundTime.FloatValue = 10.0;
        g_cvFreezeTime.IntValue = 0;
        g_cvBuyTime.FloatValue = 10.0;
        g_cvWinLimit.IntValue = 0;
        g_cvMaxRounds.IntValue = 0;
        g_cvAmmoFlashMax.IntValue = 0;
    } else {
        savedSettings = false;
        g_cvRoundTime.FloatValue = 10.0;
        g_cvFreezeTime.IntValue = 0;
        g_cvBuyTime.FloatValue = 10.0;
        g_cvWinLimit.IntValue = 0;
        g_cvMaxRounds.IntValue = 0;
        g_cvAmmoFlashMax.IntValue = ammoflashmax;
    }

    SetGameMode(Warmup);
    if (omocpreset) {
        officialMatch = true;
    }
}

public Action Cmd_OmKnife(int client, int args) {
    if (omocpreset) {
        g_cvRoundTime.FloatValue = 1.5;
        g_cvFreezeTime.IntValue = 3;
        g_cvWinLimit.IntValue = 0;
        g_cvMaxRounds.IntValue = 0;
    } else {
        RecordMatchSettings();
        g_cvRoundTime.FloatValue = 2.0;
        g_cvFreezeTime.IntValue = 3;
        g_cvWinLimit.IntValue = 0;
        g_cvMaxRounds.IntValue = 0;
    }

    SetGameMode(Knife);
    if (omocpreset) {
        officialMatch = true;
    }
}

public Action Cmd_OmStaySwitch(int client, int args) {
    g_cvRoundTime.FloatValue = 1.0;
    g_cvFreezeTime.IntValue = 0;
    g_cvBuyTime.FloatValue = 1.0;
    g_cvWinLimit.IntValue = 0;
    g_cvMaxRounds.IntValue = 0;
    g_cvAmmoFlashMax.IntValue = 0;

    SetGameMode(StaySwitch);
    if (omocpreset) {
        officialMatch = true;
    }
}

public Action Cmd_OmMatch(int client, int args) {
    if (omocpreset) {
        g_cvHalftime.IntValue = 1;
        g_cvStartMoney.IntValue = 800;
        g_cvRoundTime.FloatValue = 1.5;
        g_cvFreezeTime.IntValue = 10;
        g_cvBuyTime.FloatValue = 0.34;
        g_cvWinLimit.IntValue = 11;
        g_cvMaxRounds.IntValue = 20;
        g_cvAmmoFlashMax.IntValue = 2;
    } else if (savedSettings) {
        g_cvHalftime.IntValue = 1;
        g_cvStartMoney.IntValue = saved_startmoney;
        g_cvRoundTime.FloatValue = saved_roundtime;
        g_cvFreezeTime.IntValue = saved_freezetime;
        g_cvBuyTime.FloatValue = saved_buytime;
        g_cvWinLimit.IntValue = saved_winlimit;
        g_cvMaxRounds.IntValue = saved_maxrounds;
        g_cvAmmoFlashMax.IntValue = 2;
    }
    
    SetGameMode(Match);
    if (omocpreset) {
        officialMatch = true;
    }
}

public Action Cmd_Swap(int client, int args) {
    ChangeAllPlayersTeam();
    return Plugin_Handled;
}

public Action Cmd_SwapUser(int client, int args) {
    if (args != 1) {
        ReplyToCommand(client, "Usage: om_swapplayer <userid>");
        return Plugin_Handled;
    }
    char uid[30];
    GetCmdArg(1, uid, sizeof(uid));
    ChangePlayerTeam(GetClientOfUserId(StringToInt(uid)));

    return Plugin_Handled;
}

public Action Cmd_Pause(int client, int args) {
    g_cvServerPausable.IntValue = 1;
    int proxy;
    for (int i = 1; i <= MaxClients; i++) {
        if (IsClientInGame(i)) {
            proxy = i;
            break;
        }
    }
    FakeClientCommand(proxy, "pause");
    g_cvServerPausable.IntValue = 0;
}

public Action Cmd_Revive(int client, int args) {
    if (args != 1) {
        ReplyToCommand(client, "Usage: om_revive <userid>");
        return Plugin_Handled;
    }
    char uid[30];
    GetCmdArg(1, uid, sizeof(uid));
    CreateTimer(0.1, Timer_Respawn, GetClientOfUserId(StringToInt(uid)));

    return Plugin_Handled;
}

public Action Cmd_SetRoundTime(int client, int args) {
    if (args != 1) {
        ReplyToCommand(client, "Usage: om_setroundtime <seconds>");
        return Plugin_Handled;
    }

    char secondsString[10];
    GetCmdArg(1, secondsString, sizeof(secondsString));
    GameRules_SetPropFloat("m_fRoundStartTime", GetGameTime() - float(GameRules_GetProp( "m_iRoundTime")) + StringToFloat(secondsString));
    return Plugin_Handled;
}

public Action Cmd_UserMoney(int client, int args) {
    if (args != 2) {
        ReplyToCommand(client, "Usage: om_usermoney <userid> <money>");
        return Plugin_Handled;
    }

    char strUID[15];
    char strMoney[10];
    GetCmdArg(1, strUID, sizeof(strUID));
    GetCmdArg(2, strMoney, sizeof(strMoney));
    int UID = StringToInt(strUID);
    int money = StringToInt(strMoney);

    SetPlayerMoneyUID(UID, money);
    return Plugin_Handled;
}

public void Event_PlayerDeath(Event event, const char[] name, bool dontBroadcast) {
    if (is_warmup) {
        int victimId = event.GetInt("userid");
        CreateTimer(0.3, Timer_Respawn, GetClientOfUserId(victimId));
    }
}

public void Event_PlayerSpawn(Event event, const char[] name, bool dontBroadcast) {
    if (is_warmup) {
        int spawnId = event.GetInt("userid");
        int clientId = GetClientOfUserId(spawnId);
        SetEntProp(clientId, Prop_Send, "m_iAccount", 16000);

        //Remove C4
        if (IsClientInGame(clientId) && GetClientTeam(clientId) == CS_TEAM_T) {
            int c4bomb = GetPlayerWeaponSlot(clientId, 4);
            if (c4bomb != -1) {
                RemovePlayerItem(clientId, c4bomb);
            }
        }
    }
}

public Action:Timer_Respawn(Handle:timer, any:clientID) {
    if (clientID == 0) {
        return Plugin_Stop;
    }
    if (IsClientInGame(clientID)) {
        CS_RespawnPlayer(clientID);
    }
    return Plugin_Stop;
}

public void Event_RoundStart(Event event, const char[] name, bool dontBroadcast) {
    if (is_warmup) {
        if (is_stayswitch) {
            SetNextMap(mapname);
            PrintToChatAll("\x03 Waiting for winning team's choice. Type !stay or !switch to choose a side.");
        } else {
            SetNextMap(mapname);
            for (int ci = 1; ci <= MaxClients; ci++) {
                //Remove C4
                if (IsClientInGame(ci) && GetClientTeam(ci) == CS_TEAM_T) {
                    int c4bomb = GetPlayerWeaponSlot(ci, 4);
                    if (c4bomb != -1) {
                        RemovePlayerItem(ci, c4bomb);
                    }
                }
            }
            if (omocpreset) {
                PrintToChatAll("\x03 OMOC Major - Warmup is Live! Have Fun!");
            } else {
                PrintToChatAll("\x03 Warmup is Live! Have Fun!");
            }
        }
    }
    else if (is_knife) {
        for (int ci = 1; ci <= MaxClients; ci++) {
            if (IsClientInGame(ci) && GetClientTeam(ci) > 1) {
                for (int weapon, i = 0; i < 5; i++) {
                    if (i != 2) {
                        while ((weapon = GetPlayerWeaponSlot(ci, i)) != -1) {
                            RemovePlayerItem(ci, weapon);
                        }
                    }
                }
                SetEntProp(ci, Prop_Send, "m_ArmorValue", 100);
                SetEntProp(ci, Prop_Send, "m_bHasHelmet", 0);
                SetEntProp(ci, Prop_Send, "m_bHasDefuser", 0);
                SetEntProp(ci, Prop_Send, "m_iAccount", 0);
                EquipPlayerWeapon(ci, GetPlayerWeaponSlot(ci, 2));
            }
        }
        SetNextMap(mapname);
        if (omocpreset) {
            PrintToChatAll("\x03 OMOC Major - Knife round is Live!");
        } else {
            PrintToChatAll("\x03 Knife round is Live!");
        }
    }
    else if (is_match) {
        //Check if the match is just starting
        if ((CS_GetTeamScore(2) + CS_GetTeamScore(3)) == 0) {
            curRnd = 0;
            roundToCheckOT = -1;
            is_overtime = false;
            otHalfTimeCheck = 0;
            ResetEconomy();
            SetNextMap(mapname);
            if (officialMatch) {
                ServerCommand("om_stat 1");
                PrintToChatAll("\x03 OMOC Major - Live! Live! Live!");
                PrintToChatAll("\x03 OMOC Major - Match is Live! Good Luck!");
            } else {
                PrintToChatAll("\x03 Live! Live! Live!");
                PrintToChatAll("\x03 Match is Live! Good Luck!");
            }            
            //Check if halftime is enabled - set firsthalf
            if (halftime) {
                firsthalf = true;
            }
        } else {
            previousRoundWinner = currentRoundWinner;
            GiveRoundStartMoney();
        }
        //Check if Overtime
        if (otEnabled && (CS_GetTeamScore(2) + CS_GetTeamScore(3)) == roundToCheckOT) {
            g_cvWinLimit.IntValue = winlimit + otRounds;
        }
        //Check if halftime enabled and if it's just halftime
        if (halftime) {
            if (swap) {
                SetPistolRoundEquipments();
                SetAllPlayersMoney(startmoney);
                swap = false;
                ResetEconomy();
            }
        }
        //Check if it's the pistol round on overtime
        if (otEnabled && otResetToPistol) {
            SetPistolRoundEquipments();
            SetAllPlayersMoney(otStartMoney);
            ResetEconomy();
            otResetToPistol = false;
        }
        //Check if team swap on overtime
        if (otEnabled && otSwap) {
            SetPistolRoundEquipments();
            SetAllPlayersMoney(otStartMoney);
            ResetEconomy();
            otSwap = false;
        }
    }
}

public Action:Event_RoundEnd(Handle:event, const String:name[], bool:dontBroadcast) {
    if (is_match) {
        roundEndReason = GetEventInt(event, "reason");
        currentRoundWinner = GetEventInt(event, "winner");
        ConsecutiveModifiers();
        curRnd = CS_GetTeamScore(2) + CS_GetTeamScore(3);

        if (curRnd == maxrounds || CS_GetTeamScore(2) == winlimit || CS_GetTeamScore(3) == winlimit) {
            MatchHasEnded();
        }

        if (otEnabled) {
            if (curRnd == maxrounds - 1) {
                otHalfTimeCheck = maxrounds;
                g_cvMaxRounds.IntValue = maxrounds + (otRounds * 2);
                roundToCheckOT = curRnd + 1;
            }
            //Next round start is the first half of the overtime
            if (curRnd == roundToCheckOT && CS_GetTeamScore(2) != winlimit && CS_GetTeamScore(3) != winlimit) {
                otResetToPistol = true;
                is_overtime = true;
            }
            //Check if overtime halftime and set the swap parameter for OT
            if (halftime && is_overtime && (curRnd == otHalfTimeCheck - otRounds)) {
                ChangeAllPlayersTeam();
                SwapTeamScores();
                otSwap = true;
            }
        }
        if (halftime && firsthalf) {
            new timeleft;
            new timelimit;
            GetMapTimeLeft(timeleft);
            GetMapTimeLimit(timelimit);
            //Set the swap for players and scores
            if ((maxrounds != 0 && curRnd == (maxrounds / 2)) || (timelimit != 0 && timeleft <= (timelimit * 30))) {
                ChangeAllPlayersTeam();
                SwapTeamScores();
                swap = true;
                firsthalf = false;
            }
        }
        //Save players money state
        for (int client = 1; client <= MaxClients; client++) {
            if (IsClientInGame(client) && GetClientTeam(client) > 1) {
                playerMoney[client] = GetEntData(client, g_iAccount);
                playerAlive[client] = IsPlayerAlive(client);
            }
        }
    }
    else if (is_knife) {
        currentRoundWinner = GetEventInt(event, "winner");
        ServerCommand("om_stayswitch");
    }
    else if (is_stayswitch) {
        ServerCommand("om_match");
    }
}

public void SwapTeamScores() {
    int tmp = CS_GetTeamScore(2);
    CS_SetTeamScore(2, CS_GetTeamScore(3));
    CS_SetTeamScore(3, tmp);
    SetTeamScore(2, CS_GetTeamScore(2));
    SetTeamScore(3, CS_GetTeamScore(3));
}

public void SetPistolRoundEquipments() {
    for (int client = 1; client <= MaxClients; client++) {
        if (IsClientInGame(client) && GetClientTeam(client) > 1) {
            for (int weapon, i = 0; i < 5; i++) {
                if (i != 2 && i != 4) {
                    while ((weapon = GetPlayerWeaponSlot(client, i)) != -1) {
                        RemovePlayerItem(client, weapon);
                    }
                }
            }
            GivePlayerItem(client, (GetClientTeam(client) == 2) ? "weapon_glock" : "weapon_usp");
            SetEntProp(client, Prop_Send, "m_ArmorValue", 0);
            SetEntProp(client, Prop_Send, "m_bHasHelmet", 0);
            SetEntProp(client, Prop_Send, "m_bHasDefuser", 0);
        }
    }
}

public void SetAllPlayersMoney(int money) {
    for (int client = 1; client <= MaxClients; client++) {
        if (IsClientInGame(client) && GetClientTeam(client) > 1) {
            SetPlayerMoney(client, money);
        }
    }
}

public void SetPlayerMoney(int client, int money) {
    SetEntData(client, g_iAccount, money, 4, true);
}

public void SetPlayerMoneyUID(int userid, int money) {
    int cid = GetClientOfUserId(userid);
    SetPlayerMoney(cid, money);
}

public void ChangeAllPlayersTeam() {
    for (int i = 1; i <= MaxClients; i++) {
        ChangePlayerTeam(i);
    }
}

public void ChangePlayerTeam(int clientID) {
    if (IsClientInGame(clientID) && GetClientTeam(clientID) > 1) {
        CS_SwitchTeam(clientID, (GetClientTeam(clientID) == 2) ? 3 : 2);
    }
}

public void GiveRoundStartMoney() {
    for (int ci = 1; ci <= MaxClients; ci++) {
        if (IsClientInGame(ci)) {
            int totalMoney = CalculateBonusMoney(ci, GetClientTeam(ci));
            totalMoney = (totalMoney <= 16000) ? totalMoney : 16000;
            SetEntData(ci, g_iAccount, totalMoney);
        }
    }
}

public int CalculateBonusMoney(int ci, int team) {
    int newCash = 0;
    switch (roundEndReason) {
        //Bomb Exploded
        case 0: {
            //T Side Win Money
            if (team == 2) {
                newCash = playerMoney[ci] + 3500;
            }
            //CT Side Loss Money
            else {
                //Without 2 Consecutive Wins
                if (consec2WinCT == false) {
                    if (consecLossCT >= 5) {
                        newCash = playerMoney[ci] + 3400;
                    } else {
                        newCash = playerMoney[ci] + roundLossBonus[consecLossCT - 1];
                    }
                } else {
                    if (consecLossCT >= 4) {
                        newCash = playerMoney[ci] + 3000;
                    } else {
                        newCash = playerMoney[ci] + roundLossBonus2[consecLossCT - 1];
                    }
                }
            }
        }
        //Bomb Defused
        case 6: {
            //CT Side Win Money
            if (team == 3) {
                newCash = playerMoney[ci] + 3250;
            }
            //T Side Loss Money
            else {
                //Without 2 Consecutive Wins
                if (consec2WinT == false) {
                    if (consecLossT >= 5) {
                        newCash = playerMoney[ci] + 3400 + 800;
                    } else {
                        newCash = playerMoney[ci] + roundLossBonus[consecLossT - 1] + 800;
                    }
                } else {
                    if (consecLossT >= 4) {
                        newCash = playerMoney[ci] + 3000 + 800;
                    } else {
                        newCash = playerMoney[ci] + roundLossBonus2[consecLossT - 1] + 800;
                    }
                }
            }
        }
        //CTs Eliminated All Ts
        case 7: {
            //CT Side Win Money
            if (team == 3) {
                newCash = playerMoney[ci] + 3250;
            }
            //T Side Loss Money
            else {
                //Without 2 Consecutive Wins
                if (consec2WinT == false) {
                    if (consecLossT >= 5) {
                        newCash = playerMoney[ci] + 3400;
                    } else {
                        newCash = playerMoney[ci] + roundLossBonus[consecLossT - 1];
                    }
                } else {
                    if (consecLossT >= 4) {
                        newCash = playerMoney[ci] + 3000;
                    } else {
                        newCash = playerMoney[ci] + roundLossBonus2[consecLossT - 1];
                    }
                }
            }
        }
        //Ts Eliminated All CTs
        case 8: {
            //T Side Win Money
            if (team == 2) {
                newCash = playerMoney[ci] + 3250;
            }
            //CT Side Loss Money
            else {
                //Without 2 Consecutive Wins
                if (consec2WinCT == false) {
                    if (consecLossCT >= 5) {
                        newCash = playerMoney[ci] + 3400;
                    } else {
                        newCash = playerMoney[ci] + roundLossBonus[consecLossCT - 1];
                    }
                } else {
                    if (consecLossCT >= 4) {
                        newCash = playerMoney[ci] + 3000;
                    } else {
                        newCash = playerMoney[ci] + roundLossBonus2[consecLossCT - 1];
                    }
                }
            }
        }
        //?? All players on one side leave
        case 9: {

        }
        //CTs Won by Time Runout
        case 11: {
            //CT Side Win Money
            if (team == 3) {
                newCash = playerMoney[ci] + 3250;
            }
            //T Side Loss Money
            else {
                //T did not survive - Otherwise no money
                if (!playerAlive[ci]) {
                    //Without 2 Consecutive Wins
                    if (consec2WinT == false) {
                        if (consecLossT >= 5) {
                            newCash = playerMoney[ci] + 3400;
                        } else {
                            newCash = playerMoney[ci] + roundLossBonus[consecLossT - 1];
                        }
                    } else {
                        if (consecLossT >= 4) {
                            newCash = playerMoney[ci] + 3000;
                        } else {
                            newCash = playerMoney[ci] + roundLossBonus2[consecLossT - 1];
                        }
                    }
                }
                //T did survive the time run out
                else {
                    newCash = playerMoney[ci];
                }
            }
        }
        //??Draw
        case 15: {

        }
        default: {

        }
    }//Switch End

    return newCash;
}

public void ConsecutiveModifiers() {
    //CT Wins
    if (currentRoundWinner == 3) {
        //Set Consecutive Round Loss
        consecLossCT = 0;
        consecLossT++;
        //Set 2 Consecutive Win Bonus Modifier
        if (consec2WinCT == false && previousRoundWinner == 3) {
            consec2WinCT = true;
        }
    }
    //T Wins
    else if (currentRoundWinner == 2) {
        //Set Consecutive Round Loss
        consecLossCT++;
        consecLossT = 0;
        //Set 2 Consecutive Win Bonus Modifier
        if (consec2WinT == false && currentRoundWinner == 2 && previousRoundWinner == 2) {
            consec2WinT = true;
        }
    }
}

public void SetGameMode(int mode) {
    switch (mode) {
        case Disabled: {
            currentRoundWinner = 0;
            is_warmup = false;
            is_knife = false;
            is_stayswitch = false;
            is_match = false;
        }
        case Warmup: {
            is_warmup = true;
            is_knife = false;
            is_stayswitch = false;
            is_match = false;
        }
        case Knife: {
            currentRoundWinner = 0;
            is_warmup = false;
            is_knife = true;
            is_stayswitch = false;
            is_match = false;
        }
        case StaySwitch: {
            is_warmup = true;
            is_knife = false;
            is_stayswitch = true;
            is_match = false;
        }
        case Match: {
            currentRoundWinner = 0;
            is_warmup = false;
            is_knife = false;
            is_stayswitch = false;
            is_match = true;
        }
    }
    PhaseChangeRoutine();
}

public void PhaseChangeRoutine() {
    officialMatch = false;
    ServerCommand("sv_alltalk 1");
    ServerCommand("sm_show_damage 0");
    ServerCommand("om_stat 0");
    GetCurrentMap(mapname, 30);
    ResetEconomy();
    ServerCommand("mp_restartgame 1");
}

public void ResetEconomy() {
    consec2WinCT = false;
    consec2WinT = false;
    consecLossCT = 0;
    consecLossT = 0;
    previousRoundWinner = 0;
    ResetPlayerStates();
}

public void ResetPlayerStates() {
    for (int i = 0; i < 65; i++) {
        playerMoney[i] = 0;
        playerAlive[i] = false;
    }
}

public void MatchHasEnded() {
    if (officialMatch) {
        CreateTimer(30.0, Timer_DelayedCommand, 0);
    } else {
        currentRoundWinner = 0;
        ServerCommand("om_stat 0");
    }
    is_overtime = false;
    otHalfTimeCheck = 0;
}

public Action Timer_DelayedCommand(Handle:timer, int cmd) {
    switch (cmd) {
        case 0: {
            ServerCommand("sm_nextmap %s", mapname);
            ServerCommand("om_warmup");
        }
    }
    return Plugin_Stop;
}

public void RecordMatchSettings() {
    saved_startmoney = startmoney;
    saved_roundtime = roundtime;
    saved_buytime = buytime;
    saved_freezetime = freezetime;
    saved_maxrounds = maxrounds;
    saved_winlimit = winlimit;
    savedSettings = true;
}